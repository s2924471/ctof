package nl.utwente.di.bookQuote;

public class Translate {
    public double translateC(double celsius) {
        return (celsius * 1.8) + 32;
    }
}
