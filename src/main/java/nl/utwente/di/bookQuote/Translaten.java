package nl.utwente.di.bookQuote;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;

/**
 * Example of a Servlet that gets an ISBN number and returns the book price
 */

@WebServlet(description="Translate Servlet", urlPatterns={"/translate"})
public class Translaten extends HttpServlet {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Translate translate;

    public void init() throws ServletException {
        translate = new Translate();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String title = "Celsius to Fahrenheit";

        // Done with string concatenation only for the demo
        // Not expected to be done like this in the project
        out.println("<!DOCTYPE HTML>\n" +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Celsius: " +
                request.getParameter("celsius") + "\n" +
                "  <P>Fahrenheit: " +
                translate.translateC(Double.parseDouble(request.getParameter("celsius"))) +
                "</BODY></HTML>");
    }
}
