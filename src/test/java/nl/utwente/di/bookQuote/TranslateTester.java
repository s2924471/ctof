package nl.utwente.di.bookQuote;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TranslateTester {

    @Test
    public void testTranslate() {
        Translate t = new Translate();
        double fahrenheit = t.translateC(10.0);
        Assertions.assertEquals(50.0, fahrenheit, 0.0, "Celsius to fahrenheit");
    }
}
